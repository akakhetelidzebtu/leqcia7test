package com.btu.leqcia7projecttest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        enterButton.setOnClickListener {
            if(emailEditText.text.isEmpty() || passwordEditText.text.isEmpty()) {
                Toast.makeText(this, "Please fill all of the fields", Toast.LENGTH_LONG).show()
            }
            else{
                val response = authenticate(emailEditText.text.toString(),passwordEditText.text.toString())

                if(response.isNotEmpty()) {
                    val newIntent = Intent(this, ProfileActivity::class.java)

                    newIntent.putExtra("message", response)

                    startActivity(newIntent)
                }
                else {
                    Toast.makeText(this, "E-Mail or password is incorrect", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

    fun authenticate(username:String,password:String):String{
        return if(username == "aleksandre.kakhetelidze@btu.edu.ge" && password == "paroli1") {
            "Sandro Kakhetelidze"
        }
        else {
            ""
        }
    }
}